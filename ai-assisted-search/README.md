# POC: ai-assisted-search

## Prerequisites

- Docker
- OpenAPI Access and API key
  - Put your API key to `env.openai` file OPENAI_API_KEY=xxx
- dotenv

## Quick start

Assumes that `dotenv` has been configured and environment variables has been loaded.

**1. Start the environment**

`docker compose up`

**2. Gather the data**

`cd scripts`

`python3 scrape-to-database.py`

**3. Search**

`psql -c "insert into chat (log_name, human_query) values (gen_random_uuid(), 'Tell me about person named Roger Launius?')"`

**4. Get results**

`psql --pset=format=wrapped -c "select human_query, ai_response from chat order by ai_response_created_at desc limit 1"`

