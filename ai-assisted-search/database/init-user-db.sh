#!/bin/bash

set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  CREATE USER root;
  CREATE USER aasearch WITH PASSWORD '$AASEARCH_DB_PASSWORD';
  CREATE DATABASE aasearch;
  GRANT ALL PRIVILEGES ON DATABASE aasearch TO aasearch;
  ALTER DATABASE aasearch OWNER TO aasearch; 
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname aasearch -c "CREATE EXTENSION vector"

psql -v ON_ERROR_STOP=1 --username aasearch --dbname aasearch < /schema.pg.sql