-- Chat table. Works as a memory for the chatbot.
CREATE TABLE chat (
    id SERIAL PRIMARY KEY,
    log_name UUID NOT NULL,
    human_query VARCHAR,
    human_query_created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    ai_response TEXT,
    ai_response_created_at TIMESTAMP,
    ai_response_metadata JSON
);
-- Document table. Data found here is indexed and used for the search.
CREATE TABLE document (
    id SERIAL PRIMARY KEY,
    link VARCHAR,
    meta JSON,
    title VARCHAR,
    body TEXT,
    ingested BOOLEAN NOT NULL DEFAULT FALSE,
    
    CONSTRAINT u_document_link UNIQUE (link)
);
