#!/usr/bin/env python3

from datetime import datetime
import logging
import os
import time
import traceback
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.text_splitter import CharacterTextSplitter
from langchain.vectorstores.pgvector import PGVector
from langchain.docstore.document import Document
import psycopg2
import psycopg2.extras
import logging
import json

BATCH_SIZE = 10
VECTOR_DB = f"postgresql+psycopg2://{os.environ.get('PGHOST')}:{os.environ.get('PGPORT')}/{os.environ.get('PGDATABASE')}"


class JSONFormatter(logging.Formatter):
    def format(self, record):
        log_entry = {
            "message": record.getMessage(),
            "level": record.levelname,
            "timestamp": self.formatTime(record),
            "name": record.name,
            "pathname": record.pathname,
            "lineno": record.lineno
        }
        return json.dumps(log_entry)


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

ch = logging.StreamHandler()
ch.setFormatter(JSONFormatter())
logger.addHandler(ch)

embeddings = OpenAIEmbeddings()
document_counter: int = 0

try:
    conn = psycopg2.connect("")
except Exception as e:
    logging.error(traceback.format_exc())

logger.info("Start listening and ingesting data")

while True:
    with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as curs:
        try:
            #
            # Lock rows for processing
            #
            curs.execute(f"""
            SELECT id, meta, title, body, link FROM document
            WHERE ingested = FALSE
            ORDER BY id ASC
            LIMIT {BATCH_SIZE}
            FOR UPDATE SKIP LOCKED
            """)

            started_at = datetime.now()
            rows = curs.fetchall()
            if len(rows) == 0:
                logger.info(
                    "No more rows to ingest. Sleeping for 30 seconds...")
                time.sleep(30)
                continue

            documents = []
            ids = []
            for row in rows:
                ids.append(row["id"])
                documents.append(
                    Document(page_content=row["body"], metadata=row["meta"])
                )

            text_splitter = CharacterTextSplitter(
                chunk_size=1000, chunk_overlap=0)
            docs = text_splitter.split_documents(documents)
            document_counter += len(documents)

            logger.info(f"Ingesting documents (count={len(ids)})")

            PGVector.from_documents(
                embedding=embeddings,
                documents=docs,
                collection_name="document_embedding",
                connection_string=VECTOR_DB,
            )

            ended_at = datetime.now()
            curs.execute(
                "UPDATE document SET ingested = TRUE WHERE id IN (" + ", ".join(map(str, ids)) + ")")

            if document_counter > 0 and document_counter % 10000 == 0:
                logger.info(f"MARK Ingested {document_counter} documents")
            else:
                print(".", end="", flush=True)

            conn.commit()
            time_diff = ended_at - started_at
            logger.info(
                f"Done ingesting documents. It took {time_diff} to complete.")

        except Exception as e:
            logger.info(e)
            logger.error(traceback.format_exc())
            conn.rollback()

        finally:
            curs.close()

    time.sleep(2)
