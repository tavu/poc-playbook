import asyncio
import requests
import os
import json
import psycopg2
import string
from playwright.async_api import async_playwright
from urllib.parse import urlparse, unquote
from pathlib import Path
from PyPDF2 import PdfReader

def convert_pdf_to_text(pdf_path):
    reader = PdfReader(pdf_path)
    text = ''
    for page in reader.pages:
        page_text = page.extract_text() or ''
        # Remove non-printable characters
        page_text = ''.join(filter(lambda x: x in string.printable, page_text))
        text += page_text + '\n'
    return text

def url_to_filename(url):
    parsed_url = urlparse(url)
    domain = unquote(parsed_url.netloc).replace(':', '_').replace('/', '_')
    path = unquote(parsed_url.path.strip(
        "/")).lower().replace('/', '_').replace(' ', '_')
    return f"{domain}_{path}"

async def extract_and_save_pdf_links(url, folder='docs'):
    conn = psycopg2.connect()
    cursor = conn.cursor()
    async with async_playwright() as p:
        browser, pdf_links = await p.chromium.launch(), []
        page = await browser.new_page()
        #
        # Grap all PDF links
        #
        await page.goto(url)
        for link in await page.query_selector_all("a[href$='.pdf']"):
            href, text = await link.get_attribute("href"), await link.inner_text()
            pdf_links.append((href, text.strip()))
        await browser.close()
        os.makedirs(folder, exist_ok=True)
        for href, title in pdf_links:
            try:
                filename = url_to_filename(href)
                file_path = os.path.join(folder, filename)
                print(f"{href}")
                if not Path(file_path).exists():
                    response = requests.get(href)
                    response.raise_for_status()
                    with open(file_path, 'wb') as f:
                        f.write(response.content)
                    print(f" - Wrote to file {file_path}")
                else:
                    print(f" - Skipping GET: {file_path} already exists")
                #
                # Check if already in database
                #
                cursor.execute(
                    "SELECT id FROM document WHERE link = %s", (href,))
                row = cursor.fetchone()
                if row:
                    print(f" - Already in database with id={row[0]}")
                else:
                    #
                    # Convert PDF to text
                    #
                    pdf_text = convert_pdf_to_text(file_path)
                    print(f" - Extracted {len(pdf_text)} chars from PDF")
                    cursor.execute("INSERT INTO document (link, meta, title, body) VALUES (%s, %s, %s, %s)", (
                        href, json.dumps({"from_link": url, "link": href, "title": title}), title, pdf_text))
                    conn.commit()
                    print(" - Inserted into database")
            except requests.RequestException as e:
                print(f"Error: {e}")
            except Exception as e:
                print(f"An error occurred: {e}")
                os.remove(file_path)
                exit()
            print()
        cursor.close()
        conn.close()

#
# Starting point
#
asyncio.run(extract_and_save_pdf_links(
    'https://history.nasa.gov/series95.html'))
