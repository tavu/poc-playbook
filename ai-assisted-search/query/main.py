#!/usr/bin/env python3

"""
AI Assisted Search

Responds to user queries with a response from the vector store
"""

from langchain.vectorstores.pgvector import PGVector
from langchain.embeddings.openai import OpenAIEmbeddings
from datetime import datetime
import os
import openai
import json
import sys
import time
import psycopg2
import psycopg2.extras


def get_answer(past_messages, question, context):

    #
    # Example of using Chat API. Using model gpt-4-1106-preview which is not yet for production use, but with it context can be much larger.
    #
    messages = [{"role": "system", "content": """
As a language model filling the role of a tutor with expertise in the history and technical aspects of NASA and space exploration, your instructions would be as follows:

Provide Accurate Information: Always ensure that the facts you present about NASA and space exploration are accurate and up-to-date. When discussing historical events, give precise dates and details.

Use Technical Language Appropriately: When explaining concepts related to space and NASA's operations, use technical language accurately. However, be prepared to break down complex terms into simpler language if needed for better understanding.

Engage with Curiosity: Encourage questions and deeper exploration of topics. If a user expresses interest in a specific aspect of space or NASA's history, provide detailed information and resources.

Offer Contextual Learning: Place NASA's achievements and space missions within the broader context of space exploration history. Highlight how past discoveries and missions have influenced current space technology and future endeavors.

Utilize Visuals When Possible: If a concept is complex, consider using diagrams or requesting visuals to aid in explanation. This can help in understanding spacecraft design, orbital mechanics, and other technical aspects of space exploration.

Provide Historical Perspectives: When discussing NASA and space-related topics, offer a historical viewpoint. For instance, when explaining the Apollo missions, discuss the era's technological limits and the global political climate.

Be an Adaptive Tutor: Tailor your explanations to the user's level of understanding. Whether the user is a novice or has advanced knowledge, adjust your responses to be challenging yet comprehensible.

Guide Further Exploration: Suggest additional resources for learning, such as NASA's official website, educational publications, documentaries, and other authoritative sources.

Stimulate Critical Thinking: Encourage the user to think critically about technological challenges, the impact of space exploration on society, and the ethical considerations of space travel.

Maintain Objectivity: While NASA's achievements are numerous, also be prepared to discuss criticisms and failures as part of a comprehensive overview of its history.

By following these instructions, you will effectively fulfill the role of a knowledgeable and engaging tutor on the subjects of NASA's history and the technicalities of space exploration.
"""}]
    messages += past_messages
    messages += [
        {
            "role": "user",
            "content": f"""
Given this part of the document: '{context}' 

What is the answer to the question: '{question}'?
"""
        }
    ]
    response = openai.ChatCompletion.create(
        model="gpt-4-1106-preview",
        messages=messages,
        temperature=0,
    )
    return response['choices'][0]['message']['content'].strip()

    #
    # Example of using the Completion API. Context size here is a lot smaller so we would need to use different techniques to restrict it.
    #
    # response = openai.Completion.create(
    #     engine="text-davinci-003",
    #     prompt=f"Q: {question}\nContext: {context}\nA:",
    #     max_tokens=100
    # )
    # return response.choices[0].text.strip()

def load_past_messages(curs, log_name):
    curs.execute(
        "SELECT * FROM chat WHERE log_name = %s AND ai_response IS NOT NULL ORDER BY id ASC", (log_name,))
    rows = curs.fetchall()
    past_messages = []
    for row in rows:
        past_messages.append({"role": "user", "content": row['human_query']})
        past_messages.append(
            {"role": "assistant", "content": row['ai_response']})
    return past_messages


VECTOR_DB = f"postgresql+psycopg2://{os.environ.get('PGHOST')}:{os.environ.get('PGPORT')}/{os.environ.get('PGDATABASE')}"

conn = psycopg2.connect("")

print("Waiting for query...")
while True:
    with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as curs:
        try:
            curs.execute("""
              SELECT * FROM chat
              WHERE ai_response IS NULL 
              ORDER BY id ASC LIMIT 1 FOR UPDATE SKIP LOCKED
              """)

            rows = curs.fetchall()
            for row in rows:
                print("got query id=" + str(row['id']) + " human_query=" + row['human_query'])

                embeddings = OpenAIEmbeddings()

                db = PGVector.from_existing_index(
                    collection_name="document_embedding",
                    connection_string=VECTOR_DB,
                    embedding=embeddings
                )

                user_question = row['human_query']

                #
                # Reduced the number of results to 2 for now to lower the size of the context
                # (We have books in the database)
                # 
                started_at = datetime.now()
                relevant_docs = db.similarity_search(user_question, 4)
                ended_at = datetime.now()
                time_diff = ended_at - started_at

                print(f"db.similarity_search took {time_diff} to complete.")

                #
                # Note to here that context is limited by the defined language model
                # so that needs to be taken into account when creating the context
                #
                relevant_docs.sort(key=lambda x: len(x.page_content))

                # Not very scientific method to limit the context size
                token_count = 0
                sorted_docs = []
                for doc in relevant_docs:
                    # Estimate token count
                    token_count =+ round(len(doc.page_content)/4)
                    if (token_count < 300000):
                        # Add to context
                        sorted_docs.append(doc)
                    else:
                        print("Warning: Skipping document link=" + doc.metadata['link'] + " due to token count")
                        break

                context = "\n\n".join(
                    [doc.page_content for doc in sorted_docs])

                past_messages = load_past_messages(curs, row['log_name'])

                started_at = datetime.now()
                response = get_answer(past_messages, user_question, context)
                ended_at = datetime.now()
                time_diff = ended_at - started_at

                print(f"get_answer took {time_diff} to complete. answer={response}")

                response_metadata = {
                    f"{doc.metadata['link']}": doc.metadata for doc in relevant_docs}

                json_data = json.dumps(response_metadata)
                curs.execute("""
                  UPDATE chat
                  SET ai_response = %s, ai_response_created_at = CURRENT_TIMESTAMP, ai_response_metadata = %s
                  WHERE id = %s
                  """, (response, json_data, row['id']))

            conn.commit()
            curs.close()

        except (psycopg2.DatabaseError) as error:
            print(error)
            sys.exit(-2)

    time.sleep(2)
