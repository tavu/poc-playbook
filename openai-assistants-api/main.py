import time
import os
from openai import OpenAI
from colorama import init, Fore, Style

init(autoreset=True)

client = OpenAI()

#
# Gather and ingest data (NASA historical documents from https://history.nasa.gov/series95.html)
# OpenAI API handles transforming data and converting data to vector embeddings
#
dir = 'docs'
file_ids = []
print(Fore.GREEN + f"Ingesting files from {dir}...")
for filename in os.listdir(dir)[:20]:
    if filename.startswith('.'):
        continue
    filesize = os.path.getsize(f"{dir}/{filename}")
    print(" " + filename + " (" + str(round(filesize / 1024 / 1024)) + "MB)")
    if not os.path.exists(f"{dir}/.{filename}"):
        if filesize > 500 * 1024 * 1024:
            print(Fore.YELLOW + f"  - Skipping (file too large)")
            continue
        file = client.files.create(
            file=open(f"{dir}/{filename}", "rb"),
            purpose='assistants'
        )
        with open(f"{dir}/.{filename}", 'w') as fh:
            fh.write(file.id)
        print("  - File ID: " + file.id)
        file_ids.append(file.id)
    else:
        print(Fore.YELLOW + f"  - Skipping (already ingested)")
        with open(f"{dir}/.{filename}", 'r') as file:
            file_content = file.read()
            print("  - File ID: " + file_content)
            file_ids.append(file_content)
print(Fore.GREEN + "Done.")

#
# Create or find existing assistant
#
assistantName = "Astronomist"
assistant = None

# Find existing assistant if it exists
for t in client.beta.assistants.list():
    if t.name == assistantName:
        print(Fore.GREEN + "Found existing assistant")
        assistant = t
        break

# Create assistant if it doesn't exist
if assistant is None:
    start_time = time.time()
    assistant = client.beta.assistants.create(
        name=assistantName,
        # Instructions made by chatgpt
        instructions="""
            As a language model filling the role of a tutor with expertise in the history and technical aspects of NASA and space exploration, your instructions would be as follows:
            
            Provide Accurate Information: Always ensure that the facts you present about NASA and space exploration are accurate and up-to-date. When discussing historical events, give precise dates and details.
            
            Use Technical Language Appropriately: When explaining concepts related to space and NASA's operations, use technical language accurately. However, be prepared to break down complex terms into simpler language if needed for better understanding.

            Engage with Curiosity: Encourage questions and deeper exploration of topics. If a user expresses interest in a specific aspect of space or NASA's history, provide detailed information and resources.

            Offer Contextual Learning: Place NASA's achievements and space missions within the broader context of space exploration history. Highlight how past discoveries and missions have influenced current space technology and future endeavors.

            Utilize Visuals When Possible: If a concept is complex, consider using diagrams or requesting visuals to aid in explanation. This can help in understanding spacecraft design, orbital mechanics, and other technical aspects of space exploration.

            Provide Historical Perspectives: When discussing NASA and space-related topics, offer a historical viewpoint. For instance, when explaining the Apollo missions, discuss the era's technological limits and the global political climate.

            Be an Adaptive Tutor: Tailor your explanations to the user's level of understanding. Whether the user is a novice or has advanced knowledge, adjust your responses to be challenging yet comprehensible.

            Guide Further Exploration: Suggest additional resources for learning, such as NASA's official website, educational publications, documentaries, and other authoritative sources.

            Stimulate Critical Thinking: Encourage the user to think critically about technological challenges, the impact of space exploration on society, and the ethical considerations of space travel.

            Maintain Objectivity: While NASA's achievements are numerous, also be prepared to discuss criticisms and failures as part of a comprehensive overview of its history.

            By following these instructions, you will effectively fulfill the role of a knowledgeable and engaging tutor on the subjects of NASA's history and the technicalities of space exploration.
        """,
        tools=[{"type": "retrieval"}],
        # Latest model ChatGPT4 Turbo
        model="gpt-4-1106-preview",
        file_ids=file_ids[:20]
    )
    end_time = time.time()
    elapsed_time = end_time - start_time
    print(Fore.GREEN + f"Created assistant in {elapsed_time} seconds")

#
# Create conversation thread and make query
#
thread = client.beta.threads.create()

message = client.beta.threads.messages.create(
    thread_id=thread.id,
    role="user",
    content="Tell me about person named Roger Launius?"
)

run = client.beta.threads.runs.create(
    thread_id=thread.id,
    assistant_id=assistant.id,
    instructions="Please address the user as Jane Doe."
)

# Wait for getting response from OpenAI API
cont = True
print(Fore.GREEN + "Waiting for run to complete", end="", flush=True)
while (cont):
    run = client.beta.threads.runs.retrieve(
        thread_id=thread.id,
        run_id=run.id
    )
    if (run.status == "completed"):
        cont = False
    else:
        print(".", end="", flush=True)
        time.sleep(0.5)
print()

messages = client.beta.threads.messages.list(
    thread_id=thread.id
)

#
# Output Q/A
#

# Question
print()
print(Style.BRIGHT + Fore.BLUE + "Q:")
print(messages.data[1].content[0].text.value)

# Answer
print()
print(Style.BRIGHT + Fore.BLUE + "A:")
print(messages.data[0].content[0].text.value)
