---
marp: true
---

## Lightning Talk /

Practical and technical overview of how to

# Implement search using natural language

## Repository

You can find all this from here (after presentation)

https://gitlab.com/tavu/poc-playbook

---

# Basic structure for implementation

0 day: Initial database creation, 1+ day: Listen for updates 
- Gather the data
- Transform, normalize and ingest data
  
(In loop: Mark already fetched documents as processed and look for changes / new documents)

App functionality   
- Search
- Show results

---

# Latest OpenAI Assistants API

Getting hang of basics [openai-assistants-api/main.py](./openai-assistants-api/main.py)

### Good and easy way of getting started

or getting a reference point for benchmarking your own implementation

#### However there are limits to consider
- Default 100GB storage limit per organization
- Maximum filesize of 512MB each
- Maximum of 20 files per assistant 

---

# Example of getting around those limits

Stack [docker-compose.yml](./ai-assisted-search/docker-compose.yml) and usage examples here [README.md](./ai-assisted-search/README.md)

- postgres database with pgvector extension
  - Gathers data to one place
- index service
  - Listens for new data in database. Made by using LangChain framework
- query service
  - Transforms natural language queries to search queries
  - Makes a similarity search in vector database and getting relevant documents
  - Constructs a prompt using those documents
  - Get results from llm using that prompt

---

# What's next

- What about if target would be to use completely open-source methods
  - Mistral 7B as a language model (good for english, others maybe not)
  - LangChain as a development framework
  - QDrant as more performant vector database

- Something to look for
  - Silo AI's Poro 34B open-source llm
    - Aims to master all official EU languages

---

# Thanks!

- Use given examples as starting points to your own investigation
- Lots of tooling around, but API landscape is changing a lot
  - Guides, documentation, blog posts, udemy course materials seems to go outdated very fast so keep eye on publication date


